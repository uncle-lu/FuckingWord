# coding:utf-8

from FW.includes.words_list import create_str, create_words_list
from FW.models import Onlines

def create_online(W_count, flag_all , flag_rand , U_list):

    L = create_words_list(W_count, flag_all, flag_rand, U_list)
    Units_list = ','.join(U_list)

    O = Onlines()

    Id = len(Onlines.objects) + 1

    O.Id = Id

    O.Title = '%d_%s' % (len(L),Units_list)

    O.List = L

    O.save()

    return Id