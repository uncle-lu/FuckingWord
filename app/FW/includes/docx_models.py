# coding:utf-8

from FW.models import Words, Units, Pdfs
from FW.includes.words_list import create_str, create_words_list
import random
import io
from docx import Document
from docx.shared import Cm, Pt
from docx.oxml.ns import qn

def create_file_model_1(title,page,count):

    document = Document() 

    document.styles['Normal'].font.name = u'微软雅黑'
    document.styles['Normal']._element.rPr.rFonts.set(qn('w:eastAsia'), u'微软雅黑')

    heading = document.add_heading(title,level=1)
    heading.style.paragraph_format.space_before = Cm(0.1)
    p = document.add_paragraph(page)
    k = p.style.paragraph_format
    k.line_spacing = Cm(0.625)

    if len(page) > 40:
        section = document.sections[0]
        sectPr = section._sectPr
        cols = sectPr.xpath('./w:cols')[0]
        cols.set(qn('w:num'),'2')

    sections = document.sections
    for section in sections:
        section.top_margin = Cm(0.5)
        section.bottom_margin = Cm(0.5)
        section.left_margin = Cm(1)
        section.right_margin = Cm(1)

    Id =  len(Pdfs.objects) +1

    doc = Pdfs(Id = Id,Title='%d_%s' % (count,title))

    new = io.BytesIO()
    document.save(new)

    doc.File.new_file()
    doc.File.write(new.getvalue())
    doc.File.close()

    doc.save()

    return Id

def create_docx(W_count, flag_all , flag_rand , U_list, models):

    L = create_words_list(W_count, flag_all, flag_rand, U_list)
    
    Words_list = create_str(L)

    Units_list = ','.join(U_list)

    Id = 0

    if models == 1:
        Id = create_file_model_1(Units_list,Words_list,len(L))
    
    return Id