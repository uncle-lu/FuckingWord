# coding:utf-8

from FW.models import Words, Units, Pdfs
import random

def create_words_list(Words_count, flag_all, flag_rand, U_list):
    L = []
    one_count = int( Words_count / len(U_list) )

    for u in U_list:
        U = Units.objects(Title = u).first()

        if flag_all == True:
            for i in U.List:
                L.append({
                    'Meaning': Words.objects(Title=i).first().Meaning,
                    'Words': i
                })
        else:
            for i in range(one_count):
                k = random.choice(U.List)
                flag = True
                while flag:
                    flag2 = True
                    for i in L:
                        if i['Words'] == k:
                            flag2 = False
                            k = random.choice(U.List)
                            break
                    if flag2:
                        flag = False


                L.append({
                    'Meaning': Words.objects(Title=k).first().Meaning,
                    'Words': k
                })

    if flag_rand == True:
        random.shuffle(L)

    return L

def create_str(Words_List):
    # add number
    page = ''

    c = 1
    while c <= len(Words_List):
        page = page + u'%d.%s\n' % (c,Words_List[c-1]['Meaning'])
        c = c + 1

    return page
