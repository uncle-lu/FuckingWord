# coding:utf-8

from flask import render_template, redirect, url_for, flash, abort, send_file, request
from FW.views import Create_pdf_views
from FW.models import Units, Words, Pdfs, Onlines
from FW.includes.online_models import create_online
from FW.forms import Create_OnlineForm
from . import Online_views

@Online_views.route('/',methods=['GET','POST'])
def index():
    Up = str(request.args.get('Title',''))
    F = Create_OnlineForm()

    if F.validate_on_submit():

        U_list = F.Units_list.data.split(',')
        count = F.Words_count.data

        Id = create_online(
                int(F.Words_count.data),
                F.flag_all.data,
                F.flag_rand.data,
                U_list
                )
        return redirect(url_for('Online_views.ones',Id = Id))
    
    F.Units_list.data = Up
    F.Words_count.data = 0

    return render_template('O/index.html',title =u'生成online页面' ,F = F)

@Online_views.route('/list',methods=['GET','POST'])
def online_list():
    O = Onlines.objects().all()

    l = []

    for p in O:
        l.append({
            'Id':int(p.Id),
            'Title':p.Title
        })
    
    return render_template('O/list.html',title=u'Online lists',L = l)

@Online_views.route('/<int:Id>',methods=['GET','POST'])
def ones(Id):
    O = Onlines.objects(Id = Id).first()
    
    return render_template('O/ones.html',title=u'%d-online' % Id,name = O.Title,L = O.List)