# FuckingWords

## 这是什么?

FuckingWords 是一个使用python-flask编写的可以个性化生成纯汉译单词默写卷.

快捷生成单词表.docx,方便打印与默写.

### Demo?

39.106.102.236

### 使用说明?

开盖即用.

### 版本更新

- v1.2
	- 更新了Online单词卷功能.你可以在手机上愉快的使用我们的功能啦QwQ!
- v1.1
	- 更换了全新的生成单词卷的逻辑.
	- 添加了生成选项.更好的个性化完成单词卷生成.
	- 修复了部分bug.
	- 增添了更多bug.
- v1.0
	- 完成基本单词卷生成.
	- 编写了部分bug.

# LICENSE

The MIT License (MIT)

Copyright © 2018 uncle-lu

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
